package com.guigarage.boxfx.maven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;

@Mojo(name = "deployToBox", defaultPhase = LifecyclePhase.COMPILE, requiresDependencyResolution = ResolutionScope.COMPILE, requiresDependencyCollection = ResolutionScope.COMPILE)
public class DeployMojo extends AbstractMojo {

	@Parameter(alias = "applicationClass", property = "boxfx.applicationClass", required = true)
	private String applicationClass;

	@Parameter(alias = "applicationIcon", property = "boxfx.applicationIcon", required = true)
	private String applicationIcon;
	
	@Parameter(alias = "boxHost", property = "boxfx.boxHost", required = true)
	private String boxHost;
	
	@Parameter(alias = "boxPort", property = "boxfx.boxPort", required = true)
	private String boxPort;

	@Component
	private MavenProject project;

	@Component
	private Settings settings;

	public void execute() {
		File targetFolder = new File(project.getBuild().getDirectory());
		File appFolder = new File(targetFolder, "boxfx-app");
		appFolder.mkdirs();
		try {
			createLibFolder(appFolder);
			createJsonConf(appFolder);
			copyCompiledApp(appFolder);
			File appFile = new File(targetFolder, "box.zip");
			createArchive(targetFolder, appFolder, appFile);
			uninstall();
			upload(appFile);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void createJsonConf(File appFolder) throws IOException {
		String groupId = project.getGroupId();
		String artifactId = project.getArtifactId();
		String versionId = project.getVersion();
		String applicationName = project.getName();

		File jsonconf = new File(appFolder, "app.json");
		jsonconf.createNewFile();
		FileWriter writer = new FileWriter(jsonconf);
		writer.write("{\n");
		writer.write("\"groupId\": \"" + groupId + "\",\n");
		writer.write("\"artifactId\": \"" + artifactId + "\",\n");
		writer.write("\"version\": \"" + versionId + "\",\n");
		writer.write("\"applicationClass\": \"" + applicationClass + "\",\n");
		writer.write("\"applicationName\": \"" + applicationName + "\",\n");
		writer.write("\"applicationIcon\": \"" + applicationIcon + "\"\n");
		writer.write("}\n");
		writer.flush();
		writer.close();
	}

	private void copyCompiledApp(File appFolder) throws IOException,
			DependencyResolutionRequiredException {
		FileUtils.copyDirectory(new File(project.getBuild()
				.getOutputDirectory()), appFolder);
	}

	private void createLibFolder(File appFolder) throws IOException {
		Set<Artifact> artifacts = project.getArtifacts();
		File libsFolder = new File(appFolder, "libs");
		libsFolder.mkdirs();
		for (Artifact dep : artifacts) {
			if(!dep.getScope().equals(Artifact.SCOPE_PROVIDED) && !dep.getScope().equals(Artifact.SCOPE_TEST)) {
				System.out.println("copy " + dep.getFile() + " to " + libsFolder);
				FileUtils.copyFileToDirectory(dep.getFile(), libsFolder);
			}
		}
	}

	private void createArchive(File targetFolder, File appFolder, File zipFile)
			throws IOException {
		FileOutputStream fos = new FileOutputStream(zipFile);
		ZipOutputStream zos = new ZipOutputStream(fos);
		List<File> fileList = generateFileList(appFolder);
		for(File toZip : fileList) {
			String pathInZip = toZip.getAbsolutePath().substring(appFolder.getAbsolutePath().length()+1);
			System.out.println("add to archive " + pathInZip);
			ZipEntry ze = new ZipEntry(pathInZip);
			zos.putNextEntry(ze);
			FileInputStream in = new FileInputStream(toZip);
			IOUtils.copy(in, zos);
			in.close();
			zos.closeEntry();
		}
		zos.close();
	}

	private List<File> generateFileList(File file) {
	    List<File> fileList = new ArrayList<File>();
		if (file.isFile()) {
			fileList.add(file);
		}else if (file.isDirectory()) {
			File[] children = file.listFiles();
			for (File child : children) {
				fileList.addAll(generateFileList(child));
			}
		}
		return fileList;
	}
	
	private void uninstall() throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
	    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	    HttpPost httppost = new HttpPost("http://" + boxHost + ":" + boxPort + "/application-uninstall");
	    StringBuilder jsonBuilder = new StringBuilder();
	    jsonBuilder.append("{").append(System.lineSeparator());
	    jsonBuilder.append("\"groupId\": \"" + project.getGroupId() + "\",").append(System.lineSeparator());
	    jsonBuilder.append("\"artifactId\": \"" + project.getArtifactId() + "\"").append(System.lineSeparator());
	    jsonBuilder.append("}").append(System.lineSeparator());
	    ByteArrayEntity entity = new ByteArrayEntity(jsonBuilder.toString().getBytes());
	    httppost.setEntity(entity);
	    System.out.println("executing request " + httppost.getRequestLine());
	    HttpResponse response = httpclient.execute(httppost);
	    HttpEntity resEntity = response.getEntity();
	    httpclient.getConnectionManager().shutdown();
	}
	
	private void upload(File app) throws IOException {
		HttpClient httpclient = new DefaultHttpClient();
	    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	    HttpPost httppost = new HttpPost("http://" + boxHost + ":" + boxPort + "/application-upload");
	    ByteArrayEntity entity = new ByteArrayEntity(FileUtils.readFileToByteArray(app));
	    httppost.setEntity(entity);
	    System.out.println("executing request " + httppost.getRequestLine());
	    HttpResponse response = httpclient.execute(httppost);
	    HttpEntity resEntity = response.getEntity();
	    httpclient.getConnectionManager().shutdown();
	}
}
