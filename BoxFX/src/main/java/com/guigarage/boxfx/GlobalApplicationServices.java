package com.guigarage.boxfx;

import java.io.File;

import com.guigarage.boxfx.backgroundservice.ApplicationBackgroundRuntime;
import com.guigarage.boxfx.concurrency.ApplicationThreadHandler;
import com.guigarage.boxfx.container.ApplicationContainer;
import com.guigarage.boxfx.runtime.ApplicationRuntime;
import com.guigarage.boxfx.security.ApplicationSecurityManager;
import com.guigarage.boxfx.view.BoxView;

public class GlobalApplicationServices {

	private ApplicationBackgroundRuntime backgroundRuntime;

	private ApplicationRuntime applicationRuntime;

	private ApplicationContainer applicationContainer;

	private ApplicationThreadHandler applicationThreadHandler;

	private ApplicationSecurityManager securityManager;
	
	public GlobalApplicationServices(File homeFolder, BoxView view) {
		homeFolder.mkdirs();
		applicationThreadHandler = new ApplicationThreadHandler();
		securityManager = new ApplicationSecurityManager(applicationThreadHandler);
		securityManager.setAsDefault();
		applicationContainer = new ApplicationContainer(new File(homeFolder, "Box-Apps"), applicationThreadHandler);
		backgroundRuntime = new ApplicationBackgroundRuntime(applicationThreadHandler, view.getNotificationHandler());
		applicationRuntime = new ApplicationRuntime(view, applicationThreadHandler, backgroundRuntime);
	}
	
	public ApplicationContainer getApplicationContainer() {
		return applicationContainer;
	}
	
	public ApplicationRuntime getApplicationRuntime() {
		return applicationRuntime;
	}
	
	public ApplicationThreadHandler getApplicationThreadHandler() {
		return applicationThreadHandler;
	}
	
	public ApplicationBackgroundRuntime getBackgroundRuntime() {
		return backgroundRuntime;
	}
	
	public ApplicationSecurityManager getSecurityManager() {
		return securityManager;
	}
	
}
