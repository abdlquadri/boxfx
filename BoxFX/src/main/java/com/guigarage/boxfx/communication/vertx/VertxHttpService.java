package com.guigarage.boxfx.communication.vertx;

import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServerRequest;

public interface VertxHttpService {

	Handler<HttpServerRequest> createHandler();
}
