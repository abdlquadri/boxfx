package com.guigarage.boxfx.communication.vertx;

import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServerRequest;

public class DummyVertxHttpHandler implements Handler<HttpServerRequest>{

	@Override
	public void handle(HttpServerRequest req) {
		System.out.println("Got request: " + req.uri);
		System.out.println("Headers are: ");
		for (String key : req.headers().keySet()) {
			System.out.println(key + ":" + req.headers().get(key));
		}
		
		req.response.headers().put("Content-Type",
				"text/html; charset=UTF-8");
		req.response.end("<html><body><h1>Hello from vert.x!</h1></body></html>");
	}

}
