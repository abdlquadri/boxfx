package com.guigarage.boxfx.communication.vertx;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;

public class VertxServer {

	private static VertxServer instance;
	
	private HttpServer server;
	
	private Handler<HttpServerRequest> mainHandler;
	
	private List<Handler<HttpServerRequest>> handlers;
	
	public VertxServer() {
		System.out.println("Create Vert.x Server");
		server = Vertx.newVertx().createHttpServer();
		handlers = new CopyOnWriteArrayList<>();
		mainHandler = new Handler<HttpServerRequest>() {
			
			@Override
			public void handle(HttpServerRequest request) {
				for(Handler<HttpServerRequest> handler : handlers) {
					try {
					handler.handle(request);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				request.response.end();
			}
		};
		server.requestHandler(mainHandler).listen(getPort());
	}
	
	private int getPort() {
		return 8034;
	}
	
	public synchronized static VertxServer getInstance() {
		if(instance == null) {
			instance = new VertxServer();
		}
		return instance;
	}
	
	public void addHandler(Handler<HttpServerRequest> handler) {
		System.out.println("Add Vert.x Handler");
		handlers.add(handler);
	}
	
	public void addHttpService(VertxHttpService service) {
		addHandler(service.createHandler());
	}
}
