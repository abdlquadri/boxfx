package com.guigarage.boxfx.view;

import java.util.concurrent.Executors;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

import org.datafx.controller.ViewFactory;
import org.datafx.controller.context.ViewContext;
import org.datafx.controller.util.FxmlLoadException;

import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.runtime.ApplicationRuntime;
import com.guigarage.boxfx.view.skin.ApplicationIconController;
import com.sun.javafx.scene.traversal.Direction;

public class MainMenuController {

	@FXML
	private HBox hBox1;

	@FXML
	private HBox hBox2;

	@FXML
	private HBox hBox3;

	public ObservableList<ApplicationMetadata> allApps;

	private ApplicationRuntime applicationRuntime;

	public MainMenuController() {
		allApps = FXCollections.observableArrayList();
		allApps.addListener(new ListChangeListener<ApplicationMetadata>() {

			@Override
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends ApplicationMetadata> c) {
				updateMenu();
			}

		});
	}

	public void setApplicationRuntime(ApplicationRuntime applicationRuntime) {
		this.applicationRuntime = applicationRuntime;
	}
	
	private Node createApplicationIcon(
			final ApplicationMetadata applicationMetadata) throws FxmlLoadException {
		final ViewContext<ApplicationIconController> context = ViewFactory.getInstance().createByController(
				ApplicationIconController.class);
		context.register(applicationMetadata);
		context.getRegisteredObject(ApplicationIconController.class).init();

		context.getRootNode().setFocusTraversable(true);
		
		context.getRootNode().requestFocus();
		
		context.getRootNode().setOnKeyPressed(new EventHandler<KeyEvent>() {

			@SuppressWarnings("deprecation")
			@Override
			public void handle(KeyEvent event) {
				if(event.getCode().equals(KeyCode.ENTER)) {
					Executors.newSingleThreadExecutor().execute(new Runnable() {

						@Override
						public void run() {
							try {
								applicationRuntime
										.startApplication(applicationMetadata);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				} else if(event.getCode().equals(KeyCode.LEFT) && event.isControlDown()) {
					context.getRootNode().impl_traverse(Direction.LEFT);
				} else if(event.getCode().equals(KeyCode.RIGHT) && event.isControlDown()) {
					context.getRootNode().impl_traverse(Direction.RIGHT);
				} else if(event.getCode().equals(KeyCode.UP) && event.isControlDown()) {
					context.getRootNode().impl_traverse(Direction.UP);
				} else if(event.getCode().equals(KeyCode.DOWN) && event.isControlDown()) {
					context.getRootNode().impl_traverse(Direction.DOWN);
				}
			}
		});
		
		context.getRootNode().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				Executors.newSingleThreadExecutor().execute(new Runnable() {

					@Override
					public void run() {
						try {
							applicationRuntime
									.startApplication(applicationMetadata);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		return context.getRootNode();
	}

	private synchronized void updateMenu() {
		hBox1.getChildren().clear();
		hBox2.getChildren().clear();
		hBox3.getChildren().clear();

		int index = 0;
		for (ApplicationMetadata applicationMetadata : allApps) {
			try {
				Node icon = createApplicationIcon(applicationMetadata);
				if(index < 7) {
					hBox1.getChildren().add(icon);
				} else if(index < 14) {
					hBox2.getChildren().add(icon);
				} else {
					hBox3.getChildren().add(icon);
				}
			} catch (FxmlLoadException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void addToMenu(ApplicationMetadata applicationMetadata) {
		allApps.add(applicationMetadata);
	}

	public synchronized void removeFromMenu(ApplicationMetadata applicationMetadata) {
		allApps.remove(applicationMetadata);
	}
}
