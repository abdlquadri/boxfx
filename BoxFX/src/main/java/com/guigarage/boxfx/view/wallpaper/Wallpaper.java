package com.guigarage.boxfx.view.wallpaper;

import java.net.URL;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public enum Wallpaper {

	WP1(Wallpaper.class.getResource("wp1.png")),
	WP2(Wallpaper.class.getResource("wp2.png")),
	WP3(Wallpaper.class.getResource("wp3.png")),
	WP4(Wallpaper.class.getResource("wp4.png")),
	WP5(Wallpaper.class.getResource("wp5.png"));
	
	private URL url;
	
	private Wallpaper(URL url) {
		this.url = url;
	}
	
	public URL getUrl() {
		return url;
	}
	
	public static ObservableList<Wallpaper> getAll() {
		ObservableList<Wallpaper> ret = FXCollections.observableArrayList();
		for(Wallpaper wallpaper : Wallpaper.values()) {
			ret.add(wallpaper);
		}
		return ret;
	}
}
