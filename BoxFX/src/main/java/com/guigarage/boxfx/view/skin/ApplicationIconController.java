package com.guigarage.boxfx.view.skin;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;

import org.datafx.controller.context.FXMLViewContext;
import org.datafx.controller.context.ViewContext;

import com.guigarage.boxfx.application.ApplicationMetadata;

public class ApplicationIconController {

	@FXMLViewContext
	private ViewContext<ApplicationIconController> context;

	@FXML
	private Label applicationNameLabel;

	@FXML
	private ImageView applicationIconImageView;
	
	@FXML
	private Rectangle focusIndicator;

	public void init() {
		ApplicationMetadata metadata = context.getRegisteredObject(ApplicationMetadata.class);
		applicationNameLabel.setText(metadata.getName());
		applicationIconImageView.setImage(metadata.getIcon());
		
		focusIndicator.setVisible(false);
		
		context.getRootNode().focusedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
				if(newValue == true) {
					focusIndicator.setVisible(true);
				} else {
					focusIndicator.setVisible(false);
				}
			}
		});
	}
}
