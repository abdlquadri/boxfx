package com.guigarage.boxfx.container;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.datafx.concurrent.ConcurrentUtils;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;

import com.guigarage.boxfx.Box;
import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.communication.vertx.HttpMethods;
import com.guigarage.boxfx.communication.vertx.VertxHttpService;
import com.guigarage.boxfx.communication.vertx.VertxServer;
import com.guigarage.boxfx.container.json.DecodeException;
import com.guigarage.boxfx.container.json.JsonObject;

public class ApplicationUninstallWebService implements VertxHttpService {

	private ApplicationContainer applicationContainer;

	private Box box;
	
	public ApplicationUninstallWebService(Box box, 
			ApplicationContainer applicationContainer) {
		this.applicationContainer = applicationContainer;
		this.box = box;
	}

	public void start() {
		VertxServer.getInstance().addHttpService(this);
	}

	@Override
	public Handler<HttpServerRequest> createHandler() {
		return new Handler<HttpServerRequest>() {

			@Override
			public void handle(final HttpServerRequest req) {
				if (req.method
						.trim()
						.toLowerCase()
						.equals(HttpMethods.POST.toString().trim()
								.toLowerCase())
						&& req.path.trim().toLowerCase()
								.equals("/application-uninstall")) {
					req.bodyHandler(new Handler<Buffer>() {

						@Override
						public void handle(Buffer data) {
							try {
								byte[] jsonBytes = data.getBytes();
								String jsonString = new String(jsonBytes);

								try (@SuppressWarnings("resource")
								Scanner scanner = new Scanner(jsonString)
										.useDelimiter("\\A")) {
									String conf = scanner.next();
									JsonObject jsonObject = new JsonObject(conf);
									final ApplicationMetadata applicationMetadata = applicationContainer.find(jsonObject.getString("groupId"), jsonObject.getString("artifactId"));
									if(applicationMetadata != null) {
										ConcurrentUtils.runAndWait(new Runnable() {

											@Override
											public void run() {
												try {
													box.uninstall(applicationMetadata);
												} catch (IOException e) {
													e.printStackTrace();
												}
											}
										});
									}
								} catch (NoSuchElementException e) {
									throw new ApplicationContainerException(
											"Can't parse json", e);
								} catch (DecodeException e) {
									throw new ApplicationContainerException(
											"Can't parse json", e);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}
		};
	}

}