package com.guigarage.boxfx.concurrency;

public class ConcurrencyUtilities {

	public static boolean isChildOrEquals(Thread child, ThreadGroup applicationThreadGroup) {
		ThreadGroup parent = child.getThreadGroup();
		while(parent != null) {
			if(parent.equals(applicationThreadGroup)) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
	
	public static boolean isChildOrEquals(ThreadGroup child, ThreadGroup applicationThreadGroup) {
		if(child.equals(applicationThreadGroup)) {
			return true;
		}
		ThreadGroup parent = child.getParent();
		while(parent != null) {
			if(parent.equals(applicationThreadGroup)) {
				return true;
			}
			parent = parent.getParent();
		}
		return false;
	}
}
