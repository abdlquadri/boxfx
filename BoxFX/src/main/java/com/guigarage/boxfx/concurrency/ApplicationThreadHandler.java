package com.guigarage.boxfx.concurrency;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import com.guigarage.boxfx.application.ApplicationMetadata;

public class ApplicationThreadHandler {

	private ThreadGroup parentThreadGroup;

	private Map<ApplicationMetadata, ThreadGroup> applicationThreadGroups;
	
	private Map<ApplicationMetadata, ExecutorService> applicationExecutors;

	public ApplicationThreadHandler() {
		parentThreadGroup = Thread.currentThread().getThreadGroup();
		applicationThreadGroups = new HashMap<>();
		applicationExecutors = new HashMap<>();
	}

	public ThreadGroup getOrCreateThreadGroup(ApplicationMetadata applicationMetadata) {
		if (!applicationThreadGroups.containsKey(applicationMetadata)) {
			ThreadGroup applicationThreadGroup = new ThreadGroup(
					parentThreadGroup, applicationMetadata.getUniqueName());
			applicationThreadGroups.put(applicationMetadata,
					applicationThreadGroup);
		}
		return applicationThreadGroups.get(applicationMetadata);
	}

	public ExecutorService getOrCreateExecutorService(ApplicationMetadata applicationMetadata) {
		if(!applicationExecutors.containsKey(applicationMetadata)) {
			applicationExecutors.put(applicationMetadata, Executors.newCachedThreadPool(createThreadFactory(applicationMetadata)));
		}
		return applicationExecutors.get(applicationMetadata);
	}
	
	public ThreadFactory createThreadFactory(ApplicationMetadata applicationMetadata) {
		return new ApplicationThreadFactory(applicationMetadata.getUniqueName(), getOrCreateThreadGroup(applicationMetadata));
	}
	
	public boolean isApplicationThread(Thread t) {
		if (findApplicationForThread(t) == null) {
			return false;
		}
		return true;
	}

	public ThreadGroup getThreadGroupForApplication(Thread applicationThread) {
		ApplicationMetadata applicationMetadata = findApplicationForThread(applicationThread);
		if(applicationMetadata == null) {
			return null;
		}
		return getOrCreateThreadGroup(applicationMetadata);
	}

	public ApplicationMetadata findApplicationForThread(Thread thread) {
		for (ApplicationMetadata metadata : applicationThreadGroups.keySet()) {
			ThreadGroup applicationThreadGroup = applicationThreadGroups
					.get(metadata);
			if (ConcurrencyUtilities.isChildOrEquals(thread,
					applicationThreadGroup)) {
				return metadata;
			}
		}
		return null;
	}

	public ApplicationMetadata findApplicationForThreadGroup(
			ThreadGroup threadGroup) {
		for (ApplicationMetadata metadata : applicationThreadGroups.keySet()) {
			ThreadGroup applicationThreadGroup = applicationThreadGroups
					.get(metadata);
			if (ConcurrencyUtilities.isChildOrEquals(threadGroup,
					applicationThreadGroup)) {
				return metadata;
			}
		}
		return null;
	}

	public boolean isApplicationThreadGroup(ThreadGroup g) {
		if (findApplicationForThreadGroup(g) == null) {
			return false;
		}
		return true;
	}
	
	public void kill(ApplicationMetadata applicationMetadata) {
		applicationExecutors.remove(applicationMetadata);
		ThreadGroup group = applicationThreadGroups.get(applicationMetadata);
		if(group != null) {
			Thread[] allThreads = new Thread[0];
			group.enumerate(allThreads);
			for(Thread thread : allThreads) {
				thread.stop();
			}
		}
	}
	
	public BoxApplicationExecutor createApplicationExecutor(ApplicationMetadata applicationMetadata) {
		return new BoxApplicationExecutor(getOrCreateExecutorService(applicationMetadata), createThreadFactory(applicationMetadata));
	}

}
