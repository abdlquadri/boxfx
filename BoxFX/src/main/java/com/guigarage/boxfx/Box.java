package com.guigarage.boxfx;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import com.guigarage.boxfx.application.ApplicationException;
import com.guigarage.boxfx.application.ApplicationMetadata;
import com.guigarage.boxfx.container.ApplicationUninstallWebService;
import com.guigarage.boxfx.notification.Notification;
import com.guigarage.boxfx.view.BoxView;

public class Box extends Application {

	private BoxView boxView;

	private GlobalApplicationServices globalApplicationServices;

	private static File homeFolder;

	private void startBox(Stage stage) {
		boxView = new BoxView();
//		DefaultOverlay overlayLayer = new DefaultOverlay();
		StackPane pane = new StackPane();
		pane.getChildren().add(boxView.getView());
//		pane.getChildren().add(overlayLayer);
		final Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.setWidth(1280);
		stage.setHeight(720);
//		FlatterFX.style();
//		FlatterFX.getInstance().setOverlayLayerForScene(scene, overlayLayer);
		stage.show();

		Executors.newSingleThreadExecutor().execute(new Runnable() {

			@Override
			public void run() {
				globalApplicationServices = new GlobalApplicationServices(
						homeFolder, boxView);

				globalApplicationServices
						.getApplicationContainer()
						.getInstalledApplications()
						.addListener(
								new ListChangeListener<ApplicationMetadata>() {

									@Override
									public void onChanged(
											Change<? extends ApplicationMetadata> change) {
										while (change.next()) {

											List<? extends ApplicationMetadata> addedApplications = change
													.getAddedSubList();
											if (addedApplications != null) {
												for (ApplicationMetadata applicationMetadata : addedApplications) {
													try {
														install(applicationMetadata);
													} catch (Exception e) {
														e.printStackTrace();
														boxView.getNotificationHandler()
																.show(applicationMetadata, new Notification(
																		"Error",
																		"Can't install App!"));
													}
												}
											}

											List<? extends ApplicationMetadata> removedApplications = change
													.getRemoved();
											if (removedApplications != null) {
												for (ApplicationMetadata applicationMetadata : removedApplications) {
													try {
														uninstall(applicationMetadata);
													} catch (Exception e) {
														e.printStackTrace();
														boxView.getNotificationHandler()
																.show(applicationMetadata, new Notification(
																		"Error",
																		"Can't uninstall App!"));
													}
												}
											}

										}
									}

								});

				loadAllApplications();

				ApplicationUninstallWebService uninstallWebService = new ApplicationUninstallWebService(
						Box.this, globalApplicationServices
								.getApplicationContainer());
				uninstallWebService.start();

				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						scene.focusOwnerProperty().addListener(
								new ChangeListener<Node>() {

									@Override
									public void changed(
											ObservableValue<? extends Node> observable,
											Node oldValue, Node newValue) {
										System.out.println(newValue);
									}
								});
						boxView.show(globalApplicationServices
								.getApplicationRuntime());

					}
				});
			}
		});
	}

	private void install(ApplicationMetadata applicationMetadata)
			throws ApplicationException {
		globalApplicationServices.getBackgroundRuntime().add(
				applicationMetadata);
		globalApplicationServices.getBackgroundRuntime().start(
				applicationMetadata);
		boxView.addToMenu(applicationMetadata);
	}

	public void uninstall(ApplicationMetadata applicationMetadata)
			throws IOException {
		globalApplicationServices.getBackgroundRuntime().remove(
				applicationMetadata);
		globalApplicationServices.getApplicationThreadHandler().kill(
				applicationMetadata);
		boxView.removeFromMenu(applicationMetadata);
		globalApplicationServices.getApplicationContainer().uninstall(
				applicationMetadata);
	}

	protected void loadAllApplications() {
		globalApplicationServices.getApplicationContainer()
				.loadAllApplications();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		startBox(primaryStage);
	}

	public static void main(String[] args) {
		homeFolder = new File(args[0]);
		launch(args);
	}
}
