package com.guigarage.boxfx.util;

import java.util.UUID;

public class UID {

	public static String create() {
		return UUID.randomUUID().toString();
	}
}
