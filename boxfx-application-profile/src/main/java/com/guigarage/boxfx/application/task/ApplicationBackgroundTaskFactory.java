package com.guigarage.boxfx.application.task;

import java.util.List;

public abstract class ApplicationBackgroundTaskFactory {
	
	public abstract List<ApplicationBackgroundTask> createTasks();
}
