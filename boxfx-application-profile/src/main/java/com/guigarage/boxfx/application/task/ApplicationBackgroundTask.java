package com.guigarage.boxfx.application.task;

import javafx.util.Duration;

public interface ApplicationBackgroundTask extends Runnable{
	
	Duration getLoopDuration();

}
