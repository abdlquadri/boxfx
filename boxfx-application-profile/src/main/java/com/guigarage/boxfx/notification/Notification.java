package com.guigarage.boxfx.notification;

import com.guigarage.boxfx.application.ApplicationMetadata;

public class Notification {

	private String title;
	
	private String text;
	
	public Notification(String title, String text) {
		this.text = text;
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getText() {
		return text;
	}
}
